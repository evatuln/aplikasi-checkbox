package com.example.tugascheckbox

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.annotation.ContentView
import androidx.appcompat.app.AppCompatDelegate

class MainActivity : AppCompatActivity() {

    lateinit var cb_hair : CheckBox
    lateinit var cb_eyebrow : CheckBox
    lateinit var cb_eyes : CheckBox
    lateinit var cb_moustache : CheckBox
    lateinit var cb_beard : CheckBox
    lateinit var cb_body: CheckBox
    lateinit var cb_mask: CheckBox
    lateinit var cb_saranghae: CheckBox
    lateinit var cb_love: CheckBox

    lateinit var img_hair : ImageView
    lateinit var img_eyebrow : ImageView
    lateinit var img_eyes : ImageView
    lateinit var img_moustache : ImageView
    lateinit var img_beard : ImageView
    lateinit var img_body: ImageView
    lateinit var img_mask: ImageView
    lateinit var img_saranghae: ImageView
    lateinit var img_love: ImageView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Declare the switch from the layout file
        val btn = findViewById<Switch>(R.id.switch1)

        // set the switch to listen on checked change
        btn.setOnCheckedChangeListener { _, isChecked ->

            // if the button is checked, i.e., towards the right or enabled
            // enable dark mode, change the text to disable dark mode
            // else keep the switch text to enable dark mode
            if (btn.isChecked) {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
                btn.text = "Disable dark mode"
            } else {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
                btn.text = "Enable dark mode"
            }
        }

        cb_body = findViewById(R.id.body)
        cb_beard = findViewById(R.id.beard)
        cb_eyebrow = findViewById(R.id.eyebrow)
        cb_eyes = findViewById(R.id.eyes)
        cb_hair = findViewById(R.id.hair)
        cb_moustache = findViewById(R.id.moustache)
        cb_mask = findViewById(R.id.mask)
        cb_saranghae = findViewById(R.id.saranghae)
        cb_love = findViewById(R.id.love)

        img_body = findViewById(R.id.imagebody)
        img_beard = findViewById(R.id.imagebread)
        img_eyebrow = findViewById(R.id.imageeyebrow)
        img_eyes = findViewById(R.id.imageeyes)
        img_hair = findViewById(R.id.imagehair)
        img_moustache = findViewById(R.id.imagemoustache)
        img_mask = findViewById(R.id.imagemask)
        img_saranghae = findViewById(R.id.imagesaranghae)
        img_love = findViewById(R.id.imagelove)



        cb_body.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener(){ compoundButton: CompoundButton, b: Boolean ->
            if (cb_body.isChecked()){
                img_body.setVisibility(View.VISIBLE)
            }else{
                img_body.setVisibility(View.INVISIBLE)
            }
        })

        cb_hair.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener(){ compoundButton: CompoundButton, b: Boolean ->
            if (cb_hair.isChecked()){
                img_hair.setVisibility(View.VISIBLE)
            }else{
                img_hair.setVisibility(View.INVISIBLE)
            }
        })

        cb_eyes.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener(){compoundButton: CompoundButton, b: Boolean ->
            if (cb_eyes.isChecked()){
                img_eyes.setVisibility(View.VISIBLE)
            }else{
                img_eyes.setVisibility(View.INVISIBLE)
            }
        })

        cb_eyebrow.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener(){compoundButton: CompoundButton, b: Boolean ->
            if (cb_eyebrow.isChecked()){
                img_eyebrow.setVisibility(View.VISIBLE)
            }else{
                img_eyebrow.setVisibility(View.INVISIBLE)
            }
        })
        cb_moustache.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener(){compoundButton: CompoundButton, b: Boolean ->
            if (cb_moustache.isChecked()){
                img_moustache.setVisibility(View.VISIBLE)
            }else{
                img_moustache.setVisibility(View.INVISIBLE)
            }
        })
        cb_beard.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener(){compoundButton: CompoundButton, b: Boolean ->
            if (cb_beard.isChecked()){
                img_beard.setVisibility(View.VISIBLE)
            }else{
                img_beard.setVisibility(View.INVISIBLE)
            }
        })
        cb_mask.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener(){compoundButton: CompoundButton, b: Boolean ->
            if (cb_mask.isChecked()){
                img_mask.setVisibility(View.VISIBLE)
            }else{
                img_mask.setVisibility(View.INVISIBLE)
            }
        })

        cb_saranghae.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener(){compoundButton: CompoundButton, b: Boolean ->
            if (cb_saranghae.isChecked()){
                img_saranghae.setVisibility(View.VISIBLE)
            }else{
                img_saranghae.setVisibility(View.INVISIBLE)
            }
        })
        cb_love.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener(){compoundButton: CompoundButton, b: Boolean ->
            if (cb_love.isChecked()){
                img_love.setVisibility(View.VISIBLE)
            }else{
                img_love.setVisibility(View.INVISIBLE)
            }
        })

    }

}

